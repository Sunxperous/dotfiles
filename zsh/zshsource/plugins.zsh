# Zsh plugins
source "$HOME/dotfiles/submodules/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"
source "$HOME/dotfiles/submodules/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "$HOME/dotfiles/submodules/zsh-history-substring-search/zsh-history-substring-search.zsh"

# zsh-users/zsh-history-substring-search
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey '^[[A' history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[B' history-substring-search-down

# Zsh prompt
autoload -Uz promptinit
promptinit
source "$HOME/dotfiles/submodules/powerlevel10k/powerlevel10k.zsh-theme"

# Powerline10k
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
